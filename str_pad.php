<?php
$input = "Alien";
echo str_pad($input, 10);
echo '</br>';
echo str_pad($input, 10, "-=", STR_PAD_LEFT); 
echo '</br>';
echo str_pad($input, 10, "+", STR_PAD_BOTH); 
echo '</br>';
echo str_pad($input,  6, "___", STR_PAD_RIGHT);   
echo '</br>';
echo str_pad($input,  11, "*", STR_PAD_BOTH);                 
?>